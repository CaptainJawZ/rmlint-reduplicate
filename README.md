# rmlint reduplicate

This is a simple script, to use the leftover rmlint.json file to copy deleted duplicated files back to their original location.

The script is heavely commented, altho the only thing you need to edit is the first variable so it contains the path of your rmlint.json

I do encourage to give the script a read just to vaguely know what it's doing!

Feel free to open issues with suggestions or to ask for help
